# Welcome to SAFE

### Create Virtual Envirement in source directory and install dev requirements:
```shell
virtualenv --python python2.7 venv
source venv/bin/activate
pip install -r dev_requirements.txt
```

### Install project requirements:
```shell
pip install -r requiremnts.txt -t lib
```
### Run the project with Webapp2
```shell
dev_appserver.py .
```


